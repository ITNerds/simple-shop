<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 */

class Product_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

	
    /**
    * @sparungao
    * 06252017
    *
    * this method is use retrieve the json data from text file.
    */
    public function retrieveAllProducts() {
		
		$file = base_url('json/products.txt');
		
		$jsonData =  json_decode(file_get_contents($file));
		return $jsonData->products;
    }
	
}