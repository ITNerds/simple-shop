<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Login_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
    * @sparungao
    * 06252017
    *
    * this method is use to validate user login, then creates session foruser  if login successfully.
    */
    public function validate_login() {
		$file = base_url('json/users.txt');
		
		$jsonData =  json_decode(file_get_contents($file));
	
		
		$results = array_filter($jsonData->users, function($user) {
			
			$username = $this->security->xss_clean($this->input->post("txtUsername"));
			$password = $this->security->xss_clean($this->input->post("txtPassword"));
			return (strtolower($user->email) == strtolower($username) && strtolower($user->password) == strtolower(md5($password)));
			
		});


        if(count($results) > 0) {
        	$userInfo = "";
        	foreach($results as $user) {
        		$userInfo = new stdClass();
        		$userInfo->user_id = $user->user_id;
        		$userInfo->email = $user->email;
        		$userInfo->first_name = $user->first_name;
        		$userInfo->last_name = $user->last_name;
        		$userInfo->account_type_id = $user->account_type_id;
        		$userInfo->logged_in = true;
        		
        		
        	}
           
        	
            $this->session->set_userdata("user",$userInfo);
            $response = true;
        } else {
            $messge = array('message' => 'Invalid Username or Password','class' => 'alert alert-success fade in');
            $this->session->set_flashdata('message', $messge);
            $response = false;
        }

        return $response;
    }
}