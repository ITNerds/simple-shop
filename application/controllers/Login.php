<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		$this->load->model("login_model");
		$this->load->library('session');
        
    }

    public function index()
    {
        $this->load->view('login/index');
    }

    public function login_action() {
		

									
        $this->load->library("form_validation");
        $response = $this->login_model->validate_login();
        if($response) {
            $redirectUrl = $this->input->post("redirectUrl");
            if(isset($redirectUrl) && $redirectUrl != "") {
                redirect($redirectUrl);
            } else {
                redirect("product");
            }
        } else {
            $this->load->view('login/index');
        }
    }
    


    public function logout() {
        $this->session->sess_destroy();
        redirect("/");
    }
}
