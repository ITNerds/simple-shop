<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Order extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		//$this->load->model("order_model");
		$this->load->library('session');
        
    }


    public function process_order() {
    	$user = $this->session->userdata('user');
		
    	$products = $this->security->xss_clean($this->input->post("chkProduct"));
    	$quantity = $this->security->xss_clean($this->input->post("txtQuantity"));
    	$price = $this->security->xss_clean($this->input->post("hdPrice"));
    	$productName = $this->security->xss_clean($this->input->post("hdProductName"));
    	$productDescription = $this->security->xss_clean($this->input->post("hdProductDesc"));
    	$productCode = $this->security->xss_clean($this->input->post("hdProductCode"));
    	
    	 
    	
    	$arrayOrder = array();
    	$amount = 0;
    	foreach($products as $productId => $product) {
	    	
    			$ordersInfo = discountComputation($productId,$quantity,$price);
	    		
    		    $orders = new stdClass();
    		    $orders->product_id = $productId;
    		    $orders->quantity = $quantity[$productId];
    		    $orders->price = $ordersInfo->price;
    		    $orders->name = $productName[$productId];
    		    $orders->description = $productDescription[$productId];
    		    $orders->code = $productCode[$productId];
    		    $orders->amount = $ordersInfo->amount;
    		    $arrayOrder[] = $orders;
    		
    	}
    	
    	$this->session->set_userdata('arrayOrder',$arrayOrder);
    	redirect('Order/listOfOrder');
    	 
    }
    
    public function listOfOrder() {
    	renderTemplate("Order/index");
    }
    
    public function process_checkout() {
    	renderTemplate("Order/thank_you");
    }
    
}
