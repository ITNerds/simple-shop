<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @sparungao
 * 06252017
 * 
 * method use to render template
 * 
 */

if(!function_exists("renderTemplate")) {
    function renderTemplate($page,$data=null) {
        $ci =& get_instance();

        $ci->load->view("template/header",$data);
        $ci->load->view($page,$data);
        $ci->load->view("template/footer",$data);
    }
}



/**
* @sparungao
* 06252017
*
* this method is use to compute the discount per user type upon availing our products.
* includes the simple algorithm/business logic
* @params
* $productId,$quantity,$price
*/

if(!function_exists("discountComputation")) {
	function discountComputation($productId,$quantity,$price) {
		$ci =& get_instance();
		$user = $ci->session->userdata('user');
		
		$actualPrice  =  $price[$productId];
		$amount = $quantity[$productId] * $price[$productId];
		
		
		 
		//discount computation for university
		if($user->account_type_id==2 && $productId==1 && $quantity[$productId] > 4) {
			$amount =  ($quantity[$productId] * $price[$productId]) - 299.99;
		}if($user->account_type_id==2 && $productId==2) {
			$amount = $quantity[$productId] * 309.99;
			$actualPrice = 309.99;
		} if($user->account_type_id==2 && $productId==3  && $quantity[$productId] > 2) {
			$amount = $quantity[$productId] * 389.99;
			$actualPrice = 389.99;
		}
		 
		//discount computation for school
		if($user->account_type_id==3 && $productId==3 && $quantity[$productId] > 3) {
			$amount =  $quantity[$productId] * 379.99;
			$actualPrice =  379.99;
		}
		
		
		//discount computation for college
		if($user->account_type_id==4 && $productId==1 && $quantity[$productId] > 2) {
			 
			$amount =  ($quantity[$productId] * $price[$productId]) - $price[$productId];
		
		}
		 
		 
		//discount computation for student
		if($user->account_type_id==5 && $productId==2) {
			$amount = $quantity[$productId] * 299.99;
			$actualPrice =  299.99;
		}
		 
		
		
		
		
		$Request = new stdClass();
		$Request->amount = $amount;
		$Request->price = $actualPrice;
		
		return $Request;
	}
}