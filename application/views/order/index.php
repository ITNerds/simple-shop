
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-10 col-md-offset-1">
        	<?php
       	 $attributes = array('action' => 'post', 'id' => 'myform');
        	echo form_open("order/process_checkout",$attributes);
  		  ?>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th class="text-center">Price</th>
                        <th class="text-center">Total</th>
                    </tr>
                </thead>
                <tbody>
					<?php
					
					$products = $this->session->userdata('arrayOrder');
					if(isset($products) && count($products) > 0  && $products!=false) {
					$total = 0;
					foreach($products as $product) {
						
					
					?>
                    <tr>
                        <td class="col-sm-8 col-md-6">
                        <div class="media">
                            <div class="media-body">
                                <h4 class="media-heading"><a href="#"><?php echo $product->name; ?></a></h4>
                               <small><?php echo $product->description; ?></small><br/>
                               <b><?php echo $product->code; ?></b>
                            </div>
                        </div></td>
                        <td class="col-sm-1 col-md-1" style="text-align: center">
                       <?php echo $product->quantity; ?>
                        </td>
                        <td class="col-sm-1 col-md-1 text-center"><strong>$<?php echo $product->price; ?></strong></td>
                        <td class="col-sm-1 col-md-1 text-center"><strong>$<?php echo  $amount = $product->amount;  ?></strong></td>
                    </tr>
					<?php $total += $amount; } ?>
                    
                    <tr>
                        <td></td>
                        <td></td>
                        <td><h3>Total</h3></td>
                        <td class="text-center"><h3><strong>$<?php echo $total; ?></strong></h3></td>
                    </tr>
                    <tr>
                      <td></td>
                        <td></td>
                        <td></td>
                        <td>
                        <button type="submit" class="btn btn-success">
                            Checkout <span class="glyphicon glyphicon-play"></span>
                        </button></td>
                    </tr>
                    <?php } else {  ?>
                    <tr>
                        <td colspan="4" class="text-center">You don't have orders yet, please go to our <a  href="<?php echo site_url()."/Product/index"; ?>">Product Listing</a> to choose any of our products.</td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            </form>
        </div>
    </div>
</div>