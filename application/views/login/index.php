<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>EZ Shopping Cart Madness | Login</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url('assets/css/custom_css.min.css')?>" rel="stylesheet">
 
</head>
<body class="hold-transition login-page">
<div class="login-box">
    
    
    <?php if($this->session->flashdata('flashSuccess')) { ?>
	            <div class='alert alert-success alert-dismissable'>
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                <?php echo $this->session->flashdata('flashSuccess') ?>
	            </div>
	        <?php  } ?>
	
	        <?php if($this->session->flashdata('flashError')) { ?>
	            <div class='alert alert-danger alert-dismissable'>
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                <?php echo $this->session->flashdata('flashError') ?>
	            </div>
	        <?php } ?>
	        
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg"  style="font-size:18px;">        
        <b>EZ </b>
        <br/>Shopping Cart Madness Login
		</p>

        <?php
        $attributes = array('action' => 'post', 'id' => 'myform');
        echo form_open("login/login_action",$attributes);
        if(isset($_GET['redirect']) && $_GET['redirect'] != "") {
            echo "<input type='hidden' name='redirectUrl' id='redirectUrl' value='".$_GET['redirect']."'>";
        }
        ?>
            <?php
            if($this->session->flashdata('message')) {
                $arr_msg = $this->session->flashdata('message');
                ?>
                <div class='callout callout-danger'>
                    <?php echo $arr_msg['message']; ?>
                </div>
                <?php
            }
            ?>
            <div class="form-group has-feedback">
                <input type="text" name="txtUsername" class="form-control" placeholder="Username or Employee Number">
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="txtPassword" class="form-control" placeholder="Password">
            </div>
            <div class="row">
                <!-- /.col -->
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


<script src="<?php echo base_url('assets/js/jquery-2.2.3.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui.min.js')?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>

<script type="text/javascript">
$(document).ready(function(){
	
	$(".showForgotPasswordModal").click(function(data){
		$('#approveModal').modal();
	});


	$("#frmForgotPassword").validate();
});
</script>
</body>
</html>