<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EZ Shopping Cart Madness</title>

     <!-- Bootstrap 3.3.6 -->
 	<link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
	<script src="<?php echo base_url('assets/js/jquery-2.2.3.min.js')?>"></script>


</head>

<body>

    <div id="wrapper">
		
		
		 <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"  style="color:black !important;">EZ Shopping Cart Madness</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right" >
            <li><a  style="color:black !important;" href="<?php echo site_url()."/Product/index"; ?>">Products</a></li>
			<li><a  style="color:black !important;" href="<?php echo site_url()."/Order/listOfOrder"; ?>">Manage Order</a></li>
			<li><a style="color:black !important;"  href="<?php echo site_url()."/login/logout";?>">Logout</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
		
		
<div style="padding:1em;">
<div class="text-right">Welcome <strong><?php echo  ucfirst($this->session->userdata('user')->first_name)." ". ucfirst($this->session->userdata('user')->last_name)."!"; ?></strong></div>
<br />