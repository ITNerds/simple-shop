<style type="text/css">
.glyphicon { margin-right:5px; }
.thumbnail
{
    margin-bottom: 20px;
    padding: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
}

.item.list-group-item
{
    float: none;
    width: 100%;
    background-color: #fff;
    margin-bottom: 10px;
}
.item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
{
    background: #428bca;
}

.item.list-group-item .list-group-image
{
    margin-right: 10px;
}
.item.list-group-item .thumbnail
{
    margin-bottom: 0px;
}
.item.list-group-item .caption
{
    padding: 9px 9px 0px 9px;
}
.item.list-group-item:nth-of-type(odd)
{
    background: #eeeeee;
}

.item.list-group-item:before, .item.list-group-item:after
{
    display: table;
    content: " ";
}

.item.list-group-item img
{
    float: left;
}
.item.list-group-item:after
{
    clear: both;
}
.list-group-item-text
{
    margin: 0 0 11px;
}

.errorTxt{
  min-height: 20px;
  color:red;
  font-weight:bold;
}
.qtyErrorMsg{
  min-height: 20px;
  color:red;
  font-weight:bold;
}


</style>
<div class="container" style="width:70%;">
	<h1 class="text-center">List of our Products</h1>
    <div id="products" class="row list-group-item">
    <div class="qtyErrorMsg"></div>
    
    <?php
    	$user = $this->session->userdata('user');
    
        $attributes = array('action' => 'post', 'id' => 'productForm');
        echo form_open("order/process_order",$attributes);
    ?>
   	 <?php foreach($products as $product) {
   	 	$discountedPrice = "";
   	 	if($user->account_type_id==5 && $product->product_id==2) {
   	 		$discountedPrice = 299.99; 
   	 	}
   	 	if($user->account_type_id==2 && $product->product_id==2) {
   	 		$discountedPrice = 309.99; 
   	 	}
   	 	?>
        <div class="item  col-xs-4 col-lg-4 list-group-item">
                <div class="caption">
               		 <div class="row">
  						
               		 	<div class="col-md-12">
               		 	<div class="errorTxt"></div>
		                    <h4 class="group inner list-group-item-heading">
		                    	<input type="checkbox" name="chkProduct[<?php echo $product->product_id; ?>]" id="chkProduct"  validate="required:true, minlength:1" />
		   
		                    	 
		                    	<?php echo $product->name; ?>
		                         <input type="hidden" name="hdProductName[<?php echo $product->product_id; ?>]" id="hdProductName"  value="<?php echo $product->name; ?>" />
		                         <input type="hidden" name="hdProductDesc[<?php echo $product->product_id; ?>]" id="hdProductDesc" value="<?php echo $product->description; ?>" />
		                   		 <input type="hidden" name="hdProductCode[<?php echo $product->product_id; ?>]" id="hdProductCode" value="<?php echo $product->code; ?>" />
		                    </h4>
	                    </div>
                    </div>
                     <div class="row">
                        <div class="col-md-12">
		                    <p class="group inner list-group-item-text">
		                        <?php echo $product->description; ?> 
		                     </p>
		                </div>
		             </div>        
                     <div class="row">
                        <div class="col-md-12">
                           <label> Product Code :   </label> <?php echo $product->code; ?>
                        </div>
                    </div>
                     <div class="row">
                  
                        <div class="col-md-2" style="width:100px !important; margin:0 !important;">
                            <label> Quantity : </label>
                        </div>
                        <div class="col-md-2">
                        <input class="form-control input-sm" type="text" validate="number:true" name="txtQuantity[<?php echo $product->product_id; ?>]" id="txtQuantity" value="1" />
                        </div>
                        <div class="col-md-8">
                        &nbsp;
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                           <label> Price :   </label> $<?php echo $product->price; ?>
                           <?php if($discountedPrice!="") { ?>
                           <br />
                           <label style="color:red;"> Discounted Price :   </label> <span style="color:red">$<?php echo $discountedPrice; ?></span>
                           <?php } ?>
                           <input type="hidden" name="hdPrice[<?php echo $product->product_id; ?>]" id="hdPrice" value="<?php echo $product->price; ?>" />
                        </div>
                    </div>
                                           
                   
                </div>
        </div>
       <?php } ?>
       
         <div class="row">
         
                <div class="col-md-12 text-right">
                                 	
                    <button type="submit" class="btn btn-primary btn-flat">Submit Orders</button>
                </div>
            </div>
       </form>
    </div>
</div>


<script type="text/javascript">

/*
 * 
 * Added jquery validation for ordering, user needs to atleast choose/check atleast one product
 * And user can only input number for the quantity
 */

$(document).ready(function(){
	$('#productForm').validate({ 
        errorLabelContainer: '.errorTxt',
        errorLabelContainer: '.qtyErrorMsg'
    });
    
    $('#chkProduct').each(function() {
        $(this).rules('add', {
            required: true,
            minlength: 1,
            messages: {
                required:  "Please choose at least one product to submit order/s."
            }
        });
    });
    $('#txtQuantity').each(function() {
        $(this).rules('add', {
        	number: true,
            messages: {
            	number:  "Please input number/s only."
            }
        });
    });
});
</script>
